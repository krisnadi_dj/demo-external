$('#email_notice').hide();
$('#email_notice2').hide();
$('#password_notice').hide();
$('#captcha_notice').hide();

function refreshCaptcha(){
    $.ajax({
        type: "GET",
        url: "/resfresh-captcha",
        success: function (data) {
            $(".captcha span").html(data.captcha);
        },
    });
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function submitLogin() {
    let email = $('#email').val();
    let password = $('#password').val();
    let captcha = $('#captcha').val();

    $('#email_notice').hide();
    $('#email_notice2').hide();
    $('#password_notice').hide();
    $('#captcha_notice').hide();

    if (email === '') {
        $('#email_notice').show();
    } else if (this.validateEmail(email)) {
        $('#email_notice').hide();
    } else {
        $('#email_notice2').show();
    }

    if (password == '') {
        $('#password_notice').show();
    } else {
        $('#password_notice').hide();
    }

    if (captcha == '') {
        $('#captcha_notice').show();
    } else {
        $('#captcha_notice').hide();
    }

    if (email != '' && password != '' && captcha != '') {
        let params = {
            _token: $('meta[name="csrf-token"]').attr('content'),
            email: email,
            password: password,
            captcha: captcha
        }

        axios.post("/checklogin", params).then(res => {
            if (res.data.status === 200) {
                swal({
                    title: 'Berhasil Login',
                    text: 'Selamat datang',
                    type: 'success',
                    timer: 3000,
                    showCancelButton: false,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    closeOnClickOutside: false
                }).then(function () {
                    window.location.href = res.data.redirect_url;
                });
            } else {
                swal({
                    title: 'Perhatian !',
                    text: res.data.message,
                    type: 'warning',
                    timer: 3000,
                    showCancelButton: false,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    closeOnClickOutside: false
                }).then(function () {
                    return false;
                });
            }
        }).catch(function (error) {
            swal({
                title: 'Perhatian !',
                text: error.response.data.message,
                type: 'warning',
                timer: 3000,
                showCancelButton: false,
                showConfirmButton: false,
                allowOutsideClick: false,
                closeOnClickOutside: false
            }).then(function () {
                return false;
            });
        });
    }
}
