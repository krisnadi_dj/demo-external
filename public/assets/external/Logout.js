function signOut() {
    let params = {
        _token: $('meta[name="csrf-token"]').attr('content')
    }

    swal({
        title: "Sign-Out",
        text: "Yakin kamu mau keluar dari dashboard ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Tidak',
        confirmButtonText: 'Yakin'
    }).then((result) => {
        if (result.value) {
            axios.get("/logout", params).then(res => {
                if (res.data.status === 200) {
                    window.location.href = res.data.redirect_url;
                }
            }).catch(function (error) {
            });
        } else {
            return false;
        }
    });
}
